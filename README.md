# Plateforme RShiny de Diagnostic Trafic

[![CeCILL-C license](https://img.shields.io/badge/Licence-CeCILL--C-blue.svg)](https://cecill.info/licences.fr.html)
[![Version](https://img.shields.io/badge/Version-v1.0-green.svg)](#)
[![Maintenance](https://img.shields.io/maintenance/yes/2021)](#)

[![Essayez le en ligne !!!!](https://img.shields.io/badge/testez--le%20sur-dataviz.cerema.fr-orange)](https://dataviz.cerema.fr/diagnostic-trafic)

## Sommaire

- [Introduction](#introduction)
- [Installation](#installation)
- [Utilisation](#utilisation)
- [Bugs connus](#bugs-connus)
- [A propos](#a-propos)
---
## Introduction

Dans le cadre d'études de préfaisabilité de voies réservées au covoiturage (VR2+), le Cerema Centre-Est a créé un outil d'aide au diagnostic trafic prenant la forme d'un tableau de bord réalisé sous RShiny.

Le tableau de bord propose un ensemble d'indicateurs basés sur des données de stations de comptages (type SIREDO) prélablement mises en forme.

---
## Installation

Ici la marche à suivre pour l'installation

1. Cloner le répertoire
```shell
git clone https://gitlab.cerema.fr/centre-est/diagnostic-trafic-rshiny.git
```

---
## Utilisation

### Lancement

#### En local

1. Lancer `AppliDiagTrafic.RProj`

```shell
start AppliDiagTrafic.Rproj
```

2. Dans R Studio, ouvrir `app.R`

3. Cliquer sur "Run App"

#### En ligne

1. Aller sur [https://dataviz.cerema.fr/diagnostic-trafic](https://dataviz.cerema.fr/diagnostic-trafic)

### Importer des données

#### Sans données formatées au format de l'appli

1. 1er Onglet : formatage des données brutes [EXEMPLE](https://gitlab.cerema.fr/centre-est/mobilite/diagnostic-trafic-rshiny/-/blob/master/exemple/ex1_sanssegmentationvoies_donnees.csv)

2. 1er Onglet : Import des descriptions des boucles [EXEMPLE](https://gitlab.cerema.fr/centre-est/mobilite/diagnostic-trafic-rshiny/-/blob/master/exemple/ex1_sanssegmentationvoies_descriptif.csv)

3. Exporter les données pour une prochaine utilisation

#### Avec données préalablement traitées par l'appli

1. 2ème onglet

### Diagnostiquer

1. Naviguer dans les 3ème, 4ème et 5ème onglet (plus de documentation à venir)

---
## Bugs connus

Listing des bugs connus et liens vers les issues le cas échéant

---
## A propos

Contributeurs :
- nicolas.pele@cerema.fr
- aurelien.clairais@cerema.fr

Pour faire une suggestion, ouvrir une issue avec le tag `suggestion`




